# BIMserver Docker Image

A [Docker][docker] image for [BIMserver][bimserver] on [Tomcat][tomcat].

* [BIMserver image on DockerHub][dockerhub]


## Installation

Running BIMserver is as simple as
`sudo docker run -p 8080:8080 jimokanghanchao/bimserver`. This will run BIMserver on port 8080.

After that, you can visit BIMserver by the url `http://{Host}:{Port}/bimserver`.

You can easily forward this to port 80 using [iptables][iptables] with the command
`sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080`.

[bimserver]: http://bimserver.org/
[docker]: https://www.docker.com/
[dockerhub]: https://registry.hub.docker.com/u/urbanetic/bimserver/
[iptables]: https://help.ubuntu.com/community/IptablesHowTo
[tomcat]: https://tomcat.apache.org/
